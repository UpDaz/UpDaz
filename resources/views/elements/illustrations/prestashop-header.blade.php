<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 750.34 461.85">
    <defs>
      <linearGradient x1="608.58" y1="156.74" x2="837.2" y2="337.57" gradientTransform="matrix(1, 0, 0, -1, 0, 433.71)" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#fff"/>
        <stop offset="1" stop-color="#eee"/>
      </linearGradient>
      <linearGradient x1="168.97" y1="274.21" x2="39.85" y2="394.18" xlink:href="#Dégradé_sans_nom_2"/>
    </defs>
    <path d="M-68-83.14H832v600H-68Z" style="fill: none"/>
    <path d="M714.45,233.36c-27.72,13.75-64.51-3.93-64.51-3.93s8.18-40,35.91-53.7,64.49,3.91,64.49,3.91-8.18,40-35.89,53.72Z" style="fill: #f9f9f9"/>
    <path d="M107.25,128.14c16,9.61,39.07.72,39.07.72s-3-24.48-19.07-34.08S88.19,94,88.19,94,91.2,118.52,107.25,128.14Z" style="fill: #f9f9f9"/>
    <circle class="bubble" cx="634.15" cy="303.23" r="15.13" style="fill: #fff"/>
    <circle class="bubble" cx="91.81" cy="204.76" r="12.46" style="fill: #fff"/>
    <circle class="bubble" cx="128.95" cy="360.89" r="14.19" style="fill: #fff"/>
    <circle class="bubble" cx="599.1" cy="185.24" r="6.62" style="fill: #fff"/>
    <circle class="bubble" cx="155.12" cy="53.53" r="8.51" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="12.3" cy="368.19" r="12.3" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="494.04" cy="446.81" r="9.36" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="616.26" cy="89.05" r="10.4" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="40.47" cy="173.81" r="9.34" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="225.61" cy="62.77" r="12.43" style="fill: #e1e4e5"/>
    <ellipse cx="697.7" cy="362.44" rx="9.56" ry="7.65" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="212.67" cy="442.4" r="19.44" style="fill: #e1e4e5"/>
    <path d="M606,351h.25c1.48,21,17.08,21.31,17.08,21.31s-17.21.34-17.21,24.58c0-24.24-17.21-24.58-17.21-24.58S604.55,372,606,351ZM68.8,408.55h.13c.77,11.39,8.93,11.57,8.93,11.57s-9,.18-9,13.34c0-13.16-9-13.34-9-13.34s8.15-.18,8.93-11.57Z" style="fill: #e1e4e5"/>
    <path d="M394.83,193.73a44.84,44.84,0,0,0,3.91-18.15c0-23.73-18.11-43-40.44-43s-40.44,19.24-40.44,43a44.38,44.38,0,0,0,5.34,20.95" style="fill: #fff;fill-rule: evenodd"/>
    <path d="M167.63,276.55c-3.71-10.63-2.21-21.88,5.16-29.27,11-11.05,30.78-9.23,44.09,4.06L231,265.43" style="fill: none;stroke: #fff;stroke-linecap: round;stroke-linejoin: round;stroke-width: 9px"/>
    <path d="M504.36,149.27v45.8c14.2,11.62,25.47,26.11,32.16,42.7H553.8c10.92,0,19.78,8,19.78,17.93v55c0,9.9-8.86,17.93-19.78,17.93H527.87a114.4,114.4,0,0,1-43.29,39.29v32.44c0,9.9-8.86,17.94-19.78,17.94H437.38c-10.92,0-11.62-8-11.62-17.94l-.51-17.93H309.57V398c0,9.9,3.82,17.93-7.09,17.93H276.91c-10.91,0-19.77-8-19.77-17.93V354.84c-24.23-19.71-39.56-48.21-39.56-80,0-59.43,53.12-107.6,118.67-107.6h89.51a41.73,41.73,0,0,0,23.48-7.15,62.4,62.4,0,0,1,35.34-10.78Z" style="fill: none;stroke: #fff;stroke-linecap: square;stroke-width: 5px;fill-rule: evenodd"/>
    <path d="M487,238.81a3.57,3.57,0,0,1,0,4.69,2.76,2.76,0,0,1-4.21,0,3.57,3.57,0,0,1,0-4.69,2.81,2.81,0,0,1,4-.24,1.36,1.36,0,0,1,.24.24" style="fill: none;stroke: #fff;stroke-linecap: round;stroke-linejoin: round;stroke-width: 16px"/>
    <circle cx="398.74" cy="62.04" r="19.44" style="fill: #e1e4e5"/>
    <path d="M558.47,20.8h.25c1.48,21,17.08,21.31,17.08,21.31s-17.21.34-17.21,24.58c0-24.24-17.21-24.58-17.21-24.58S557,41.79,558.47,20.8ZM21.23,78.37h.13c.77,11.39,8.93,11.57,8.93,11.57s-9,.18-9,13.34c0-13.16-9-13.34-9-13.34s8.15-.18,8.93-11.57Z" style="fill: #e1e4e5"/>
  </svg>
  