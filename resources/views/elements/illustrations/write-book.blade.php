<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 900 600">
    <defs>
      <linearGradient x1="660.72" y1="304.7" x2="888.23" y2="484.66" gradientTransform="matrix(1, 0, 0, -1, 0, 600)" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#fff"/>
        <stop offset="1" stop-color="#eee"/>
      </linearGradient>
      <linearGradient x1="358.18" y1="324.87" x2="229.69" y2="444.26" xlink:href="#a"/>
    </defs>
    <path d="M0,0H900V600H0Z" style="fill: none"/>
    <path d="M461.52,411.6c-51-39-128-45.86-188.1-20.64-16.56,6.92-36-2.53-36-18.13v-238c0-12.77,6.84-25,18.82-32.8,62-40.33,149.29-35.53,205.23,14.39C517.45,66.5,604.78,61.7,666.74,102.05c12,7.79,18.82,20,18.82,32.78v238c0,15.61-19.39,25.08-35.95,18.14-60.14-25.21-137.14-18.33-188.1,20.66h0Z" style="fill: none;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4px"/>
    <path d="M237.47,212.79V450.68c0,15.6,19.39,25,36,18.13,60.14-25.22,137.14-18.34,188.1,20.62,51-39,128-45.84,188.09-20.64,16.56,6.93,35.95-2.53,35.95-18.13V212.79m-224,198.81V116" style="fill: none;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4px"/>
    <path d="M459.56,369.28l60.66-54.06L486,159.41,353,247.53l6,81" style="fill: #fff;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4px"/>
    <path d="M358.43,328.59l101.36,41L436.18,428l-101.36-41Z" style="fill: #fff;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4px;fill-rule: evenodd"/>
    <path d="M437.23,279.85,486,159.4" style="fill: none;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4px"/>
    <path d="M766.08,251.9c-27.59,13.68-64.2-3.91-64.2-3.91s8.14-39.77,35.74-53.44,64.18,3.9,64.18,3.9-8.14,39.76-35.72,53.45Z" style="fill: url(#a)"/>
    <path d="M296.76,243.91c16,9.58,38.89.73,38.89.73s-3-24.36-19-33.92S277.8,210,277.8,210,280.8,234.34,296.76,243.91Z" style="fill: url(#b)"/>
    <circle class="bubble" cx="723.76" cy="362.49" r="15.06" style="fill: #fff"/>
    <circle class="bubble" cx="190.96" cy="266.21" r="12.4" style="fill: #fff"/>
    <circle class="bubble" cx="209.68" cy="411" r="14.12" style="fill: #fff"/>
    <circle class="bubble" cx="626.94" cy="224.64" r="6.59" style="fill: #fff"/>
    <circle class="bubble" cx="203.87" cy="92.94" r="8.47" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="98.24" cy="381.76" r="12.24" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="573" cy="496.5" r="9.31" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="615.97" cy="143.21" r="10.35" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="121.58" cy="215.77" r="9.3" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="331.88" cy="135.76" r="12.37" style="fill: #e1e4e5"/>
    <ellipse cx="784.49" cy="444.45" rx="9.52" ry="7.61" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="348.47" cy="500.51" r="19.35" style="fill: #e1e4e5"/>
    <path d="M651,287.48h.25c1.47,20.88,17,21.2,17,21.2s-17.12.34-17.12,24.47c0-24.13-17.13-24.47-17.13-24.47s15.53-.32,17-21.2ZM149.82,458.42H150c.77,11.34,8.89,11.51,8.89,11.51s-9,.19-9,13.29c0-13.1-8.95-13.29-8.95-13.29s8.11-.17,8.89-11.51Z" style="fill: #e1e4e5"/>
  </svg>
  