<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 634 395.3" xml:space="preserve">
<style type="text/css">
	.books-0{fill:none;}
	.books-1{fill:url(#SVGID_1_);}
	.books-2{fill:#EBB100;}
	.books-3{fill:url(#SVGID_00000016038775762853560510000003881755274614844588_);}
	.books-4{fill:#584A1D;}
	.books-5{fill:url(#SVGID_00000152953373692821425800000000878726519101810854_);}
	.books-6{fill:url(#SVGID_00000098217850526803052080000016259268378671571360_);}
	.books-7{fill:#E1E4E5;}
	.books-8{fill:url(#SVGID_00000122684629852749674150000012643891742497842078_);}
	.books-9{fill:none;stroke:#E1E4E5;stroke-width:3.312;stroke-linecap:round;stroke-linejoin:round;}
	.books-10{fill:none;stroke:#E1E4E5;stroke-width:4.206;stroke-linecap:round;stroke-linejoin:round;}
	.books-11{fill:none;stroke:#E1E4E5;stroke-width:5.42;stroke-linecap:round;stroke-linejoin:round;}
</style>
<path class="books-0" d="M-132-81.7h900v600h-900V-81.7z"/>
<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="283.7275" y1="-156.1975" x2="272.3684" y2="819.3865" gradientTransform="matrix(1 0 0 -1 0 437.0245)">
	<stop  offset="0" style="stop-color:#FFFFFF"/>
	<stop  offset="1" style="stop-color:#EEEEEE"/>
</linearGradient>
<circle class="books-1" cx="279.2" cy="202.5" r="184.2"/>
<path class="books-2" d="M102.9,300.1h401.8c15.3,0,27.8-11.3,27.8-25.2v-15c0-13.9-12.4-25.2-27.8-25.2H102.9
	c18.4,18.2,18.7,46.1,0.7,64.7L102.9,300.1z"/>
<linearGradient id="SVGID_00000180352237312558583930000003427364356863074188_" gradientUnits="userSpaceOnUse" x1="222.0333" y1="116.1819" x2="222.3783" y2="249.7449" gradientTransform="matrix(1 0 0 -1 0 437.0245)">
	<stop  offset="0" style="stop-color:#FFFFFF"/>
	<stop  offset="1" style="stop-color:#EEEEEE"/>
</linearGradient>
<path style="fill:url(#SVGID_00000180352237312558583930000003427364356863074188_);" d="M108.5,242.1h202.2
	c13.9,0,25.2,11.3,25.2,25.2s-11.3,25.2-25.2,25.2H108.5C117.9,277.1,117.9,257.6,108.5,242.1z"/>
<path class="books-4" d="M121,163.1c-0.9-1.5,0.5-3.3,2.5-3.3h320c25.1,0,45.4,16.8,45.4,37.5c0,20.7-20.3,37.5-45.4,37.5H123.4
	c-2,0-3.4-1.8-2.5-3.3C133.5,209.9,133.5,184.7,121,163.1L121,163.1z"/>
<linearGradient id="SVGID_00000049904338804324554110000014910345543343398816_" gradientUnits="userSpaceOnUse" x1="244.8606" y1="176.0836" x2="245.3096" y2="333.9276" gradientTransform="matrix(1 0 0 -1 0 437.0245)">
	<stop  offset="0" style="stop-color:#FFFFFF"/>
	<stop  offset="1" style="stop-color:#EEEEEE"/>
</linearGradient>
<path style="fill:url(#SVGID_00000049904338804324554110000014910345543343398816_);" d="M123.5,167.9h214.7
	c15.9,0,28.7,12.9,28.7,28.7v2.2c0,15.9-12.9,28.7-28.7,28.7H123.1C132.7,208.4,132.8,187,123.5,167.9L123.5,167.9z"/>
<linearGradient id="SVGID_00000039829342259332540630000009954430916735621038_" gradientUnits="userSpaceOnUse" x1="390.3765" y1="240.3829" x2="391.0344" y2="429.8109" gradientTransform="matrix(1 0 0 -1 0 437.0245)">
	<stop  offset="0" style="stop-color:#FFFFFF"/>
	<stop  offset="1" style="stop-color:#EEEEEE"/>
</linearGradient>
<path style="fill:url(#SVGID_00000039829342259332540630000009954430916735621038_);" d="M510.4,156.5H303.9
	c-18.3,0-33.2-16-33.2-35.8S285.6,85,303.9,85h206.5c-15.8,19.8-16.4,48.9-1.5,69.5L510.4,156.5z"/>
<path class="books-2" d="M273.5,120.8c0-19.7,15-35.8,33.5-35.8h207.2c1,0,1.7-0.8,1.7-1.9c0-1-0.8-1.8-1.7-1.8H185.5
	c-20.4,0-37,17.7-37,39.5s16.6,39.5,37,39.5h328.7c1,0,1.7-0.8,1.7-1.8c0-1-0.8-1.9-1.7-1.9H307
	C288.5,156.5,273.5,140.5,273.5,120.8L273.5,120.8z"/>
<path class="books-7" d="M241.9,199.2c0-4.1,5.2-7.5,11.7-7.5h25.8c-4.9,0-8.9,2.5-8.9,5.7l0.1,80.4l-14.2-10.3l-14.5,10.3L241.9,199.2
	L241.9,199.2z"/>
<path class="books-4" d="M3,299.9h597.1c18.5,0,33.6,15,33.6,33.6v28.2c0,18.5-15,33.6-33.6,33.6H3l0.3-0.3
	C28.9,368.4,28.7,326.3,3,299.9z"/>
<linearGradient id="SVGID_00000011730243141665758480000013072362633787639736_" gradientUnits="userSpaceOnUse" x1="189.7575" y1="18.1747" x2="190.1516" y2="196.0307" gradientTransform="matrix(1 0 0 -1 0 437.0245)">
	<stop  offset="0" style="stop-color:#FFFFFF"/>
	<stop  offset="1" style="stop-color:#EEEEEE"/>
</linearGradient>
<path style="fill:url(#SVGID_00000011730243141665758480000013072362633787639736_);" d="M13.6,314h319.2c18.5,0,33.6,15,33.6,33.6
	c0,18.5-15,33.6-33.6,33.6H13.6C25.3,360.4,25.3,334.8,13.6,314z"/>
<circle class="books-9 bubble" cx="108.5" cy="59.8" r="5.5"/>
<circle class="books-7 bubble" cx="520.8" cy="191.1" r="4.8"/>
<path class="books-10 bubble" d="M451.4,48.9l-14.6,5.7l0.9-15.6l14.6-5.7L451.4,48.9z"/>
<path class="books-7 bubble" d="M68.4,266.2c3.9-3.4,5-10.2,5-10.2s-7.5-0.5-11.4,2.9c-3.9,3.4-5.6,7.8-3.8,9.8S64.5,269.6,68.4,266.2
	L68.4,266.2z M570.1,273.8c-5.9-5.2-7.6-15.5-7.6-15.5s11.4-0.8,17.3,4.4c5.9,5.2,8.5,11.8,5.8,14.9C583,280.7,576,279,570.1,273.8z
	"/>
<circle class="books-7 bubble" cx="35.5" cy="203.4" r="9.6"/>
<circle class="books-7 bubble" cx="570.5" cy="224.8" r="6.7"/>
<path class="books-11 bubble" d="M64.2,134.7L54,124.5l10.2-10.2l10.2,10.2L64.2,134.7z M553.2,130.7l10.2-10.2l-10.2-10.2L543,120.5
	L553.2,130.7z"/>
</svg>
