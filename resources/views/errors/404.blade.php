@extends('layouts.default')

@section('content')
    <div class="w-full bg-blue pt-8">
        <div class="container mx-auto">
                <img src="{{ asset('img/illustrations/404.svg')}}" class="w-1/2 mx-auto" alt="Illustration présentation" title="404 erreur"/>
            </div>
        </div>
    </div>
@endsection
