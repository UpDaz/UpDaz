<div id="references">
    <div class="w-full text-center bg-blue-dark px-8 md:px-16 py-16 md:py-24">
        <h2 class="text-white text-3xl mb-10">
            Ils m'ont fait confiance
        </h2>
        @include('elements.home.references-updaz')
    </div>
    <div class="w-full block md:flex items-stretch justify-between">
        @include('elements.home.references-awam')
        @include('elements.home.references-kazoart')
    </div>
</div>
