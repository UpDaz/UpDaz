<div id="articles" class="px-8 md:px-16 py-16 md:py-24 bg-blue">
    <p class="font-title bold text-3xl text-white mb-6">Les dernières news développement, Laravel, Webflow et Prestashop</p>
    <x-last-articles />
</div>
