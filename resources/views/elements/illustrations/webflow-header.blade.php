<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 668 485.74">
  <defs>
    <linearGradient x1="137.94" y1="88.14" x2="-9.4" y2="6.91" gradientTransform="matrix(1, 0, 0, -1, 0, 472.3)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#fff"/>
      <stop offset="1" stop-color="#eee"/>
    </linearGradient>
  </defs>
  <path d="M-103-63.85H797v600H-103Z" style="fill: none"/>
  <path d="M91.07,430.74c13.51-11.67,12.32-35.2,12.32-35.2S80.3,391,66.8,402.64s-12.34,35.19-12.34,35.19S77.55,442.42,91.07,430.74Z" style="fill: #f9f9f9"/>
  <circle class="bubble" cx="164.8" cy="403.89" r="24.91" style="fill: #fff"/>
  <circle class="bubble" cx="329.8" cy="43.57" r="19.63" style="fill: #fff"/>
  <circle class="bubble" cx="600.84" cy="61.38" r="16.36" style="fill: #fff"/>
  <circle class="bubble" cx="400.79" cy="223.08" r="7.63" style="fill: #e1e4e5"/>
  <circle class="bubble" cx="271.82" cy="223.08" r="7.63" style="fill: #e1e4e5"/>
  <circle class="bubble" cx="557.46" cy="417.71" r="9.81" style="fill: #e1e4e5"/>
  <circle class="bubble" cx="142.07" cy="225.23" r="5.97" style="fill: #e1e4e5"/>
  <circle class="bubble" cx="25.57" cy="364.6" r="7.66" style="fill: #e1e4e5"/>
  <circle class="bubble" cx="450.91" cy="95.69" r="9.14" style="fill: #e1e4e5"/>
  <circle class="bubble" cx="264.45" cy="89.19" r="6.02" style="fill: #e1e4e5"/>
  <circle class="bubble" cx="236.37" cy="396.14" r="7.7" style="fill: #e1e4e5"/>
  <circle class="bubble" cx="515.27" cy="47.59" r="12.65" style="fill: #e1e4e5"/>
  <path d="M590,341.52h.29C592,365.71,610,366.08,610,366.08s-19.84.39-19.84,28.35c0-28-19.84-28.35-19.84-28.35S588.28,365.71,590,341.52ZM197.5,0h.32c1.85,26.69,21.39,27.1,21.39,27.1s-21.55.43-21.55,31.28c0-30.85-21.54-31.28-21.54-31.28S195.65,26.69,197.5,0Z" style="fill: #e1e4e5"/>
  <path d="M381.11,394.18l28.41-57.6-71.26-104L267,336.58l28.41,57.6" style="fill: none;stroke: #fff;stroke-miterlimit: 10;fill-rule: evenodd"/>
  <path d="M381.11,394.18l28.41-57.6-71.26-104L267,336.58l28.41,57.6" style="fill: none;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4.874000072479248px"/>
  <path d="M295,394.36h86.42v49.72H295Z" style="fill: #fff;stroke: #fff;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4.874000072479248px;fill-rule: evenodd"/>
  <path d="M398,61.69h0a7.1,7.1,0,0,1-7.1-7.1h0a7.1,7.1,0,0,1,7.1-7.1h0a7.1,7.1,0,0,1,7.11,7.1h0A7.1,7.1,0,0,1,398,61.69Z" style="fill: none;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4.874000072479248px"/>
  <path d="M338.24,335.29V232.6" style="fill: none;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4.874000072479248px"/>
  <path d="M294.61,137.39C224.07,155,171.73,218.52,171.73,294.48" style="fill: none;stroke: #e1e4e5;stroke-width: 4.874000072479248px"/>
  <path d="M301.89,137.44l-166.07-.17" style="fill: none;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4.874000072479248px"/>
  <path d="M108.89,164.27h0A27,27,0,0,1,82,137.34h0a26.94,26.94,0,0,1,26.93-26.93h0a26.93,26.93,0,0,1,26.93,26.93h0a26.93,26.93,0,0,1-26.93,26.93Zm237.89,5.49H319.85a18,18,0,0,1-18-17.95V124.88a18,18,0,0,1,18-18h26.93a18,18,0,0,1,18,18v26.93A18,18,0,0,1,346.78,169.76ZM185.2,357.32H158.27a18,18,0,0,1-18-17.95V312.44a18,18,0,0,1,18-18H185.2a18,18,0,0,1,18,18v26.93A17.94,17.94,0,0,1,185.2,357.32Z" style="fill: none;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4.874000072479248px"/>
  <path d="M372.32,137.39C442.86,155,495.19,218.52,495.19,294.48" style="fill: none;stroke: #e1e4e5;stroke-width: 4.874000072479248px"/>
  <path d="M365,137.44l166.07-.17" style="fill: none;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4.874000072479248px"/>
  <path d="M558,164.27h0A26.94,26.94,0,0,0,585,137.34h0A26.93,26.93,0,0,0,558,110.41h0a26.93,26.93,0,0,0-26.93,26.93h0A26.93,26.93,0,0,0,558,164.27Zm-237.88,5.49h26.93A18,18,0,0,0,365,151.81V124.88a18,18,0,0,0-17.95-18H320.15a18,18,0,0,0-18,18v26.93A18,18,0,0,0,320.15,169.76ZM481.73,357.32h26.93a18,18,0,0,0,17.95-17.95V312.44a18,18,0,0,0-17.95-18H481.73a18,18,0,0,0-18,18v26.93A17.94,17.94,0,0,0,481.73,357.32Z" style="fill: none;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4.874000072479248px"/>
  <path d="M13.87,91.68l14.86,14.87M58.46,47.09,73.32,62M28.73,76.82l9.91,9.91m5-24.77,9.92,9.9M73.32,32.23l9.91,9.91m5-24.77,9.91,9.91" style="fill: none;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4.2230000495910645px"/>
  <path d="M6.56,114.11A10.71,10.71,0,0,1,6.56,99L95.48,10.06a10.71,10.71,0,0,1,15.13,0l24.51,24.52a10.7,10.7,0,0,1,0,15.12L46.2,138.62a10.71,10.71,0,0,1-15.13,0Z" style="fill: none;stroke: #e1e4e5;stroke-linecap: round;stroke-linejoin: round;stroke-width: 4.2230000495910645px"/>
</svg>
