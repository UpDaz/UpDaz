<table class="w-full table-auto border border-collapse border-slate-300 text-left">
    <tbody class="bg-slate-100">
        <tr>
            <td class="border border-slate-300 py-8 px-8 min-w-[250px]">
                <ul>
                    <li class="mb-2">
                        <div class="flex items-start">
                            @include('elements.svg-icons.check')
                            <span class="leading-[1.2]">
                                Création de la boutique en ligne
                                <br/>
                                <span class="text-xs">(installation de Prestashop avec sa base de données, accès à la boutique spécifique)</span>
                            </span>
                        </div>
                    </li>
                    <li class="mb-2">
                        <div class="flex items-start">
                            @include('elements.svg-icons.check')
                            <span class="leading-[1.2]">
                                Installation, adaptation et vérification du thème
                            </span>
                        </div>
                    </li>
                    <li class="mb-2">
                        <div class="flex items-start">
                            @include('elements.svg-icons.check')
                            <span class="leading-[1.2]">
                                Optimisation des performances
                            </span>
                        </div>
                    </li>
                    <li class="mb-2">
                        <div class="flex items-start">
                            @include('elements.svg-icons.check')
                            <span class="leading-[1.2]">
                                Optimisation SEO
                            </span>
                        </div>
                    </li>
                    <li class="mb-2">
                        <div class="flex items-start">
                            @include('elements.svg-icons.check')
                            <span class="leading-[1.2]">
                                Formation à la gestion des produits et des commandes
                            </span>
                        </div>
                    </li>
                </ul>
            </td>
            <td class="border border-slate-300 py-8 px-8 text-center">
                <span class="font-title text-blue font-bold text-xl">
                    2500€
                </span>
            </td>
        </tr>
        <tr>
            <td class="border border-slate-300 py-8 px-8">
                <div class="flex items-start">
                    @include('elements.svg-icons.plus')
                    <span class="leading-[1.2]">
                        Installation d'un module, vérification de son fonctionnement
                    </span>
                </div>
            </td>
            <td class="border border-slate-300 py-8 px-8 text-center whitespace-nowrap">
                <span class="font-title text-blue font-bold text-xl">
                    100€
                </span>
                / module
                <br/>
                <span class="text-xs">(+ prix du module)</span>
            </td>
        </tr>
        <tr>
            <td class="border border-slate-300 py-8 px-8">
                <div class="flex items-start">
                    @include('elements.svg-icons.plus')
                    <span class="leading-[1.2]">
                        Développement spécifique d’un module
                    </span>
                </div>
            </td>
            <td class="border border-slate-300 py-8 px-8 text-center whitespace-nowrap">
                <span class="font-title text-blue font-bold text-xl">
                    Sur devis
                </span>
            </td>
        </tr>
        <tr>
            <td class="border border-slate-300 py-8 px-8">
                <div class="flex items-start">
                    @include('elements.svg-icons.plus')
                    <span class="leading-[1.2]">
                        Intégration sur mesure 
                    </span>
                </div>
            </td>
            <td class="border border-slate-300 py-8 px-8 text-center whitespace-nowrap">
                <span class="font-title text-blue font-bold text-xl">
                    Sur devis 
                </span>
            </td>
        </tr>
    </tbody>
</table>
