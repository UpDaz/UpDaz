<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 900 600">
    <defs>
      <linearGradient x1="457.9" y1="-256.1" x2="441.74" y2="1132.36" gradientTransform="matrix(1, 0, 0, -1, 0, 600)" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#fff"/>
        <stop offset="1" stop-color="#eee"/>
      </linearGradient>
      <linearGradient x1="623.13" y1="213.69" x2="510.58" y2="719.42" xlink:href="#a"/>
      <linearGradient x1="702.98" y1="275.25" x2="899.18" y2="430.45" xlink:href="#a"/>
      <linearGradient x1="237.68" y1="165.69" x2="-12.41" y2="398.06" xlink:href="#a"/>
    </defs>
    <path d="M0,0H900V600H0Z" style="fill: none"/>
    <circle cx="451.43" cy="300" r="262.12" style="fill: url(#a)"/>
    <path d="M708.47,240.2c-12.62,22.54-40.28,25.63-84.58,21.35-33.32-3.23-63.71-5.72-97-23.89a186.7,186.7,0,0,1-55.26-46.6c-14.6-18.09-35-38.73-25-59.24,13.73-28.18,93.13-51.91,170.24-13.1,84.69,42.64,104,99.46,91.62,121.48Z" style="fill: url(#b)"/>
    <path d="M793.83,287.32c-23.79,11.8-55.36-3.38-55.36-3.38s7-34.29,30.82-46.08,55.35,3.36,55.35,3.36-7,34.3-30.81,46.1Z" style="fill: url(#c)"/>
    <path d="M118.14,373.56c31.08,18.62,75.68,1.4,75.68,1.4s-5.84-47.41-36.94-66-75.65-1.43-75.65-1.43S87.06,354.93,118.14,373.56Z" style="fill: url(#d)"/>
    <circle class="bubble" cx="747.03" cy="208.25" r="12.99" style="fill: #ebb100"/>
    <circle class="bubble" cx="257.82" cy="281.82" r="14.61" style="fill: #ebb100"/>
    <circle class="bubble" cx="159.93" cy="195.93" r="12.18" style="fill: #ebb100"/>
    <circle class="bubble" cx="635.64" cy="259.25" r="5.68" style="fill: #ebb100"/>
    <circle class="bubble" cx="668.43" cy="512.81" r="7.31" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="157.75" cy="436.95" r="10.55" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="279.79" cy="167.3" r="8.03" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="669.74" cy="168.79" r="8.93" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="181.93" cy="270.13" r="8.02" style="fill: #e1e4e5"/>
    <circle class="bubble" cx="385.55" cy="99.92" r="10.67" style="fill: #e1e4e5"/>
    <ellipse cx="725.74" cy="372.6" rx="8.21" ry="6.57" style="fill: #e1e4e5"/>
    <circle cx="570.97" cy="99.43" r="16.69" style="fill: #e1e4e5"/>
    <path d="M788,327.26h.22c1.27,18,14.66,18.29,14.66,18.29s-14.77.29-14.77,21.1c0-20.81-14.77-21.1-14.77-21.1S786.7,345.27,788,327.26ZM248.45,502.36h.19c1.12,16.3,13,16.55,13,16.55s-13.11.26-13.11,19.1c0-18.84-13.1-19.1-13.1-19.1S247.32,518.66,248.45,502.36Z" style="fill: #e1e4e5"/>
    <rect x="221.13" y="128.3" width="461.79" height="344.34" rx="4.19" style="fill: #fff;stroke: #e1e4e5;stroke-width: 4px"/>
    <path d="M404.57,248v24m96-24v24m35.8,108.38s-31.44-30.14-83.88-30.14-83.87,30.14-83.87,30.14" style="fill: none;stroke: #ebb100;stroke-linecap: round;stroke-linejoin: round;stroke-width: 33.79800033569336px"/>
    <rect x="221.13" y="128.3" width="461.79" height="40.04" rx="4.19" style="fill: #fff;stroke: #e1e4e5;stroke-width: 4px"/>
    <rect x="237.14" y="141.65" width="13.35" height="13.35" rx="6.67" style="fill: #fff;stroke: #e1e4e5;stroke-width: 4.193999767303467px"/>
    <rect x="261.17" y="141.65" width="13.35" height="13.35" rx="6.67" style="fill: #fff;stroke: #e1e4e5;stroke-width: 4.193999767303467px"/>
    <rect x="285.19" y="141.65" width="13.35" height="13.35" rx="6.67" style="fill: #fff;stroke: #e1e4e5;stroke-width: 4.193999767303467px"/>
  </svg>
  