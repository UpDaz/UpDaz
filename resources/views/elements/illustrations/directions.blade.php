<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 631 473" style="enable-background:new 0 0 631 473;" xml:space="preserve">
<style type="text/css">
	.directions-0{fill:none;}
	.directions-1{fill:url(#SVGID_1_);}
	.directions-2{fill:url(#SVGID_00000045618076428918059530000015621257011141430676_);}
	.directions-3{fill:url(#SVGID_00000142166204478297288860000013754723113939702437_);}
	.directions-4{fill:url(#SVGID_00000040545036737725713810000016484566543209410729_);}
	.directions-5{fill:url(#SVGID_00000127029589365273595820000014684652896351009450_);}
	.directions-6{fill:url(#SVGID_00000007422717705001655470000013749447273696479154_);}
	.directions-7{fill:#EBB100;}
	.directions-8{fill:#E1E4E5;}
	.directions-9{fill:none;stroke:#EBB100;stroke-width:12;stroke-linecap:round;stroke-linejoin:round;}
	.directions-10{fill:#FFFFFF;}
</style>
<path class="directions-0" d="M-133-40h900v600h-900V-40z"/>
<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="404.1505" y1="-190.5722" x2="162.2406" y2="900.8448" gradientTransform="matrix(1 0 0 -1 0 520.4094)">
	<stop  offset="0" style="stop-color:#FFFFFF"/>
	<stop  offset="1" style="stop-color:#EEEEEE"/>
</linearGradient>
<path class="directions-1" d="M560.8,314c3.6,66.1-125.7,200.7-285.1,150.9c-74.1-23.2-67.9-61.6-124.2-112.2c-33.6-30.2-81.9-53-95.3-96.9
	c-11.6-37.9-4.5-78.6,15.6-105.1c45.2-59.6,136.5-100.3,226.5-6.5C388.2,238,551.5,143.4,560.8,314L560.8,314z"/>
<linearGradient id="SVGID_00000174562870231756258620000007595968022957137067_" gradientUnits="userSpaceOnUse" x1="440.2846" y1="206.6358" x2="328.1636" y2="710.4598" gradientTransform="matrix(1 0 0 -1 0 520.4094)">
	<stop  offset="0" style="stop-color:#FFFFFF"/>
	<stop  offset="1" style="stop-color:#EEEEEE"/>
</linearGradient>
<path style="fill:url(#SVGID_00000174562870231756258620000007595968022957137067_);" d="M525.3,168.2
	c-12.6,22.4-40.1,25.5-84.3,21.3c-33.2-3.2-63.5-5.7-96.7-23.8c-23.2-12.7-41.6-29.8-55.1-46.4c-14.5-18-34.9-38.6-24.9-59
	C278.1,32.2,357.2,8.5,434,47.2C518.4,89.7,537.6,146.3,525.3,168.2z"/>
<linearGradient id="SVGID_00000105415441579529825960000003745861005716022943_" gradientUnits="userSpaceOnUse" x1="169.9009" y1="-57.4991" x2="73.7529" y2="387.1379" gradientTransform="matrix(1 0 0 -1 0 520.4094)">
	<stop  offset="0" style="stop-color:#FFFFFF"/>
	<stop  offset="1" style="stop-color:#EEEEEE"/>
</linearGradient>
<path style="fill:url(#SVGID_00000105415441579529825960000003745861005716022943_);" d="M205.1,442.8c-29.3-41-59.3-84.6-98.6-106
	c-40.5-22.1-48-8.5-50.8,13.2c-2.8,21.7,9.7,73.1,65,102.4C176,481.6,232.6,481.2,205.1,442.8z"/>
<linearGradient id="SVGID_00000077318647044079398140000004864492080203338654_" gradientUnits="userSpaceOnUse" x1="538.079" y1="307.971" x2="672.325" y2="371.393" gradientTransform="matrix(1 0 0 -1 0 520.4094)">
	<stop  offset="0" style="stop-color:#FFFFFF"/>
	<stop  offset="1" style="stop-color:#EEEEEE"/>
</linearGradient>
<path style="fill:url(#SVGID_00000077318647044079398140000004864492080203338654_);" d="M595.6,203c-15.3,3.6-31.6-9.4-31.6-9.4
	s8.7-18.9,24-22.5c15.3-3.6,31.5,9.4,31.5,9.4S611,199.3,595.6,203L595.6,203z"/>
<linearGradient id="SVGID_00000163754466893122247890000010039792940584767121_" gradientUnits="userSpaceOnUse" x1="242.7052" y1="440.4876" x2="115.6162" y2="574.4986" gradientTransform="matrix(1 0 0 -1 0 520.4094)">
	<stop  offset="0" style="stop-color:#FFFFFF"/>
	<stop  offset="1" style="stop-color:#EEEEEE"/>
</linearGradient>
<path style="fill:url(#SVGID_00000163754466893122247890000010039792940584767121_);" d="M180.2,43c16.1,11.1,40.8,3.3,40.8,3.3
	s-1.5-25.8-17.7-36.9c-16.1-11.1-40.8-3.4-40.8-3.4S164.1,31.9,180.2,43L180.2,43z"/>
<linearGradient id="SVGID_00000006665666132538194200000011067569954288921766_" gradientUnits="userSpaceOnUse" x1="545.1475" y1="407.1487" x2="456.9185" y2="545.4547" gradientTransform="matrix(1 0 0 -1 0 520.4094)">
	<stop  offset="0" style="stop-color:#FFFFFF"/>
	<stop  offset="1" style="stop-color:#EEEEEE"/>
</linearGradient>
<path style="fill:url(#SVGID_00000006665666132538194200000011067569954288921766_);" d="M496.9,70.6c12.2,12.4,35,9.8,35,9.8
	s3-22.8-9.2-35.2c-12.2-12.4-35-9.8-35-9.8S484.7,58.1,496.9,70.6L496.9,70.6z"/>
<ellipse class="directions-7 bubble" cx="530.7" cy="349.4" rx="13" ry="12.4"/>
<ellipse class="directions-7 bubble" cx="443.7" cy="415.3" rx="14.7" ry="15.3"/>
<ellipse class="directions-7 bubble" cx="87.3" cy="411.6" rx="17.5" ry="17"/>
<circle class="directions-7 bubble" cx="82.2" cy="196.2" r="6.2"/>
<circle class="directions-7 bubble" cx="555.5" cy="102.2" r="11.9"/>
<ellipse class="directions-8 bubble" cx="515.9" cy="457.9" rx="11.9" ry="11.3"/>
<ellipse class="directions-8 bubble" cx="15.4" cy="309.8" rx="12.4" ry="11.3"/>
<circle class="directions-8 bubble" cx="141.6" cy="468.7" r="4"/>
<circle class="directions-8 bubble" cx="533.7" cy="195.7" r="5.7"/>
<circle class="directions-8 bubble" cx="83" cy="267.9" r="5.5"/>
<circle class="directions-8 bubble" cx="127.4" cy="190.4" r="7.4"/>
<ellipse class="directions-8 bubble" cx="539.1" cy="273.6" rx="5.7" ry="4.5"/>
<circle class="directions-8 bubble" cx="301.3" cy="28.8" r="11.5"/>
<path class="directions-8" d="M620.5,271.3h0.1c0.9,12.4,10.1,12.6,10.1,12.6s-10.2,0.2-10.2,14.5c0-14.3-10.2-14.5-10.2-14.5
	S619.6,283.7,620.5,271.3z M134.6,75.6h0.3c1.5,21.7,17.4,22.1,17.4,22.1s-17.5,0.3-17.5,25.4c0-25.1-17.5-25.4-17.5-25.4
	S133.1,97.4,134.6,75.6L134.6,75.6z"/>
<path class="directions-7" d="M420.2,69H241.5c-8.7,0-16.9,3-23.6,8.2l-47.4,37.9c-19,15.1-19,43.9,0,59.1l47.4,37.9
	c6.7,5.4,15.1,8.2,23.6,8.2h178.7c20.9,0.1,37.8-16.8,37.9-37.7c0-0.1,0-0.1,0-0.2v-75.7C458.1,85.9,441.2,69,420.2,69z
	 M194.8,241.3h178.7c8.7,0,16.9,3,23.6,8.2l47.4,37.9c19,15.1,19,43.9,0,59.1L397,384.3c-6.7,5.4-15.1,8.2-23.6,8.2H194.8
	c-20.9,0.1-37.8-16.8-37.9-37.7c0-0.1,0-0.1,0-0.2V279C156.9,258.2,173.8,241.3,194.8,241.3L194.8,241.3z"/>
<path class="directions-9" d="M307.3,261.6v-42.3 M307.3,457.6v-64.9 M242.4,457.6h129.8"/>
<path class="directions-10" d="M316.9,99.1l9,13.8c1.4,2.2,4.6,4.4,7,5l16.1,4c10,2.5,12.7,10.9,6.2,18.7l-10.6,12.6c-1.6,2-2.8,5.6-2.7,8.1
	l0.9,16.3c0.6,10.1-6.6,15.2-16.1,11.5l-15.5-6.1c-2.4-0.9-6.3-0.9-8.7,0l-15.5,6.1c-9.5,3.7-16.8-1.6-16.1-11.5l0.9-16.3
	c0.2-2.5-1.1-6.2-2.7-8.1l-10.6-12.6c-6.5-7.8-3.8-16.2,6.2-18.7l16.1-4c2.5-0.6,5.7-3,7-5l9-13.8C302.5,90.7,311.4,90.7,316.9,99.1
	L316.9,99.1z M316.9,274.9l9,13.8c1.4,2.2,4.6,4.4,7,5l16.1,4c10,2.5,12.7,10.9,6.2,18.7L344.6,329c-1.6,2-2.8,5.6-2.7,8.1l0.9,16.3
	c0.6,10.1-6.6,15.2-16.1,11.5l-15.5-6.1c-2.4-0.9-6.3-0.9-8.7,0l-15.5,6.1c-9.5,3.7-16.8-1.6-16.1-11.5l0.9-16.3
	c0.2-2.5-1.1-6.2-2.7-8.1l-10.6-12.6c-6.5-7.8-3.8-16.2,6.2-18.7l16.1-4c2.5-0.6,5.7-3,7-5l9-13.8
	C302.5,266.5,311.4,266.5,316.9,274.9L316.9,274.9z"/>
<ellipse class="directions-7" cx="312.5" cy="145.9" rx="67.5" ry="64.5"/>
<ellipse class="directions-7" cx="304" cy="324.4" rx="67.5" ry="64.5"/>
</svg>
