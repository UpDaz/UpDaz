<div class="">
    <h3 class="text-orange text-xl">
        Sommaire
    </h3>
    <ul class="list-decimal list-inside">
        <li>
            <a href="{{ route('laravel') }}#presentation" class="hover:underline">
                Laravel, qu’est-ce que c’est ?
            </a>
        </li>
        <li>
            <a href="{{ route('laravel') }}#quelle-utilisation" class="hover:underline">
                Quelle utilisation ?
            </a>
        </li>
        <li>
            <a href="{{ route('laravel') }}#accompagnement" class="hover:underline">
                Mon accompagnement
            </a>
        </li>
        <li>
            <a href="{{ route('laravel') }}#quel-budget" class="hover:underline">
                Quel budget ?
            </a>
            <ul class="list-disc list-outside ml-8">
                <li>
                    <a href="{{ route('laravel') }}#design" class="hover:underline">
                        Le design
                    </a>
                </li>
                <li>
                    <a href="{{ route('laravel') }}#developpement" class="hover:underline">
                        Le développement
                    </a>
                </li>
                <li>
                    <a href="{{ route('laravel') }}#contenu" class="hover:underline">
                        Le contenu
                    </a>
                </li>
                <li>
                    <a href="{{ route('laravel') }}#depenses-fonctionnement" class="hover:underline">
                        Les dépenses de fonctionnement
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('laravel') }}#references" class="hover:underline">
                Références
            </a>
        </li>
        <li>
            <a href="{{ route('laravel') }}#contact" class="hover:underline">
                Contact
            </a>
        </li>
    </ul>
</div>
