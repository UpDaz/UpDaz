<table class="w-full table-auto border border-collapse border-slate-300 text-left">
    <tbody class="bg-slate-100">
        <tr>
            <td class="border border-slate-300 py-8 px-8 min-w-[250px]">
                <ul>
                    <li class="mb-2">
                        <div class="flex items-start">
                            @include('elements.svg-icons.check')
                            <span class="leading-[1.2]">
                                Création d'une landing page / page d'accueil
                            </span>
                        </div>
                    </li>
                    <li class="mb-2">
                        <div class="flex items-start">
                            @include('elements.svg-icons.check')
                            <span class="leading-[1.2]">
                                Mise en place d'un thème
                            </span>
                        </div>
                    </li>
                    <li class="mb-2">
                        <div class="flex items-start">
                            @include('elements.svg-icons.check')
                            <span class="leading-[1.2]">
                                Personnalisation du contenu de la page
                            </span>
                        </div>
                    </li>
                    <li class="mb-2">
                        <div class="flex items-start">
                            @include('elements.svg-icons.check')
                            <span class="leading-[1.2]">
                                Mise en place d'un formulaire de contact
                            </span>
                        </div>
                    </li>
                    <li class="mb-2">
                        <div class="flex items-start">
                            @include('elements.svg-icons.check')
                            <span class="leading-[1.2]">
                                Optimisation
                                <x-tooltip color="#001A9E">
                                    <x-slot:message>
                                        <span class="block p-6">
                                            User Experience - Expérience utilisateur
                                        </span>
                                    </x-slot>
                                    <x-slot:content>
                                        <i>UX</i>
                                    </x-slot>
                                </x-tooltip>
                            </span>
                        </div>
                    </li>
                    <li class="mb-2">
                        <div class="flex items-start">
                            @include('elements.svg-icons.check')
                            <span class="leading-[1.2]">
                                Optimisation du référencement naturel (SEO)
                            </span>
                        </div>
                    </li>
                </ul>
            </td>
            <td class="border border-slate-300 py-8 px-8 text-center">
                <span class="font-title text-blue font-bold text-xl">
                    1000€
                </span>
            </td>
        </tr>
        <tr>
            <td class="border border-slate-300 py-8 px-8">
                <div class="flex items-start">
                    @include('elements.svg-icons.plus')
                    <span class="leading-[1.2]">
                        Création d'une page supplémentaire avec contenu personnalisé
                    </span>
                </div>
            </td>
            <td class="border border-slate-300 py-8 px-8 text-center whitespace-nowrap">
                <span class="font-title text-blue font-bold text-xl">
                    250€
                </span>
                / page
            </td>
        </tr>
        <tr>
            <td class="border border-slate-300 py-8 px-8">
                <div class="flex items-start">
                    @include('elements.svg-icons.plus')
                    <span class="leading-[1.2]">
                        Ajout du système de traduction
                    </span>
                </div>
            </td>
            <td class="border border-slate-300 py-8 px-8 text-center whitespace-nowrap">
                <span class="font-title text-blue font-bold text-xl">
                    500€
                </span>
                / langue
            </td>
        </tr>
        <tr>
            <td class="border border-slate-300 py-8 px-8">
                <div class="flex items-start">
                    @include('elements.svg-icons.plus')
                    <span class="leading-[1.2]">
                        Mise en place d'enregistrements CMS avec appel spécifique sur le site 
                    </span>
                </div>
            </td>
            <td class="border border-slate-300 py-8 px-8 text-center whitespace-nowrap">
                <span class="font-title text-blue font-bold text-xl">
                    500 €  
                </span>
                / collection
            </td>
        </tr>
    </tbody>
</table>
